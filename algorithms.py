from math import gcd, sqrt, floor, log
from random import random

###returns the least common multiple
def lcm(num1, num2):
    return round(abs(num1 * num2)/gcd(num1, num2))

###modInv function, stolen from https://stackoverflow.com/a/29762148
modInv = lambda A, n,s=1,t=0,N=0: (n < 2 and t%N or modInv(n, A%n, t, s-A//n*t, N or n),-1)[n<1]

def randint(min, max):
    return min+int(random()*(max-min))

def bigrandint(binlength):
    total = 1
    while binlength > 512:
        total = (total << 512) + randint(0, 1<<512)
        binlength -= 512
    total = (total << binlength) + randint(0, 1<<binlength)
    return total


###return g**x % n
def modpow(g, x, n):
    val = 1
    for i in range(floor(log(x, 2)), -1, -1):
        if (x & (1 << i)) != 0:
            val = val * g % n
        if i != 0:
            val = val * val % n
    return val

##returns the euclidean distance between two points in 2 or 3 dimensions
def distance(p1, p2):
    if len(p1) == len(p2):
        return sqrt(sum([(a - b)**2 for a, b in zip(p1, p2)]))
    raise Exception('Invalid Arguments')

def chain(points):
    sum = 0
    for i in range(len(points)-1):
        sum += distance(points[i], points[i+1])
    return sum

def seq(func, inputs):
    out = [func(x) for x in inputs]
    return out

def bico(n, k):
    return int(factorial(n)/(factorial(k)*factorial(n-k)))

def average(values):
    return sum(values)/len(values)
