from math import floor, log, ceil

###returns the r most significant bits of n
def msb(n, r):
    return n >> max(binlen(n) - r, 0)

###returns the r least significant bits of n
def lsb(n, r):
    return n % (1 << r)

###returns the bits of n between start and end counted from the right
def bits(n, start, end):
    return msb(lsb(n, start), start-end)

###returns the amount of bits used to store this number
def binlen(n):
    if n == 0:
        return 1
    return floor(log(n, 2)) + 1

###returns a byte array of an int with leading zero padding
def asbytearr(n):
    out = []
    step = ceil(binlen(n)/8)
    for i in range(step):
        shift = ((step-1-i)*8)
        out.append((n & (0xff << shift)) >> shift)
    return out

###returns a byte formatted binary representation of n
def binary(n):
    barr = asbytearr(n)
    fmt = '{:08b} ' * len(barr)
    print(fmt.format(*barr))

###returns a byte formatted hexadecimal reprepresentation of n
def hexdec(n):
    barr = asbytearr(n)
    fmt = '{:02X} ' * len(barr)
    print(fmt.format(*barr))
