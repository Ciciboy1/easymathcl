from math import floor, log
from algorithms import randint, modpow

supernumbers = ['\u2070', '\u00B9', '\u00B2', '\u00B3', '\u2074', '\u2075', '\u2076', '\u2077', '\u2078', '\u2079']

###returns wheter or not n is a Prime number
def isprime(n):
    if n <= 1:
        return False
    elif n <= 3:
        return True
    elif n % 2 == 0 or n % 3 == 0:
        return False
    i = 5
    while i * i <= n:
        if n % i == 0 or n % (i + 2) == 0:
            return False
        i = i + 6
    return True

###checks whether n is prime by going through the Miller-Rabin test
def isprobprime(n, k=10):
    if n < 10:
        return isprime(10)
    if n % 2 == 0:
        return False
    if k > n-3 or k == 0:
        k = n-3
    t = 0
    m = n - 1
    while m % 2 == 0:
        t += 1
        m = m >> 1
    #print("t, m, k=", t, m, k)
    for i in range(k):
        chosen = []
        a = randint(2, n-1)
        while a in chosen:
            a = randint(2, n-1)
        chosen.append(a)
        z = modpow(a, m, n)
    #    print("a, z=", a, z)
        if z == 1 or z == n-1:
            continue
        for j in range(t):
            z = modpow(z, 2, n)
    #        print("z=", z)
            if z is (n-1):
                break
        if z == (n-1):
            continue
        else:
            return False
    return True

###returns the next Prime number after n
def nextprime(n):
    k = n + 1
    if(k < 2):
        return 2
    if(k == 2):
        return 3
    if(k % 2 == 0):
        k += 1
    while not isprime(k):
        if k % 6 == 1:
            k += 4
        else:
            k += 2
    return k

###returns a list of all primes between start and end
def primelist(start, end):
    k = nextprime(start)
    primes = []
    while(k <= end):
        primes.append(k)
        k = nextprime(k)
    return primes

### returns a Dictionary of all Prime Factors
def pfactor(n):
    factors = dict()
    value = n
    factor = 2
    factors.update({2:0})
    while not value == 1:
        remainder = value % factor
        if remainder == 0:
            value = value/factor
            factors[factor] = factors[factor] + 1
        else:
            factor = nextprime(factor)
            factors.update({factor : 0})
    return {key: value for key, value in factors.items() if not value == 0}

###This is only for good visuals and does not produce usable function output
###returns all prime factors of n
def fact(n):
    factors = pfactor(n)
    print("".join(str(i) + supernumbers[j] + " \u00D7 " for i, j in factors.items())[:-3])


###eulers totient function
def phi(n):
    if isprime(n):
        return n-1
    factors = pfactor(n)
    val = 1
    for i, j in factors.items():
        val = val * (i**(j-1)) * (i-1)
    return val

#returns whether g is a primitive root in Zp
def primRoot(g, p):
    if not isprime(p):
        return False
    factors = pfactor(p-1)
    for i, j in factors.items():
        if modpow(g, (p-1)/i, p) == 1:
            return False
    return True
