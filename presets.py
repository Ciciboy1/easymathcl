from math import *

from algorithms import *
from tables import *
from primes import *
from base import *
from bitutil import *

arctan = atan
arcsin = asin
arccos = acos
rad = radians
deg = degrees
dist = distance
avg = average
origin = (0, 0, 0)
print("Free Math tool")
