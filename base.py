import os

# clears the console window, but does not delete imports or variables
def clear():
    os.system('cls' if os.name == 'nt' else 'clear')

# runs a pip install with the specified module
def install(module):
    try:
        import pip
        pip.main(['install', module])
        del pip
    except:
        print("Install pip for Access to this command")
